//package com.demo;


import java.util.*;
import java.lang.Math;

class Example {
	
	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	
	int n=153;
	int p=(n+"").length();
	int num=n;
	int result=0;
	while(num!=0) {
		int d=num%10;
		result+=(Math.pow(d, p));
		num/=10;
	}
	if(result==n)
		System.out.println("Armstrong Number");
	else
		System.out.println("Not Armstrong Number");
	}
}
